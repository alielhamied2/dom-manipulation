function renderListsAndSelectDropdown() {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const taskListDropdown = document.getElementById("taskList");
  taskListDropdown.innerHTML = "";
  lists.forEach((list) => {
    const option = document.createElement("option");
    option.value = list.title;
    option.textContent = list.title;
    taskListDropdown.appendChild(option);
  });
}

// Function to render lists
// Function to render lists
function renderLists() {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const tasksContainer = document.getElementById("tasksContainer");
  tasksContainer.innerHTML = "";
  lists.forEach((list) => {
    const listDiv = document.createElement("div");
    listDiv.className = "col-3 card-header mb-3 me-4";
    listDiv.innerHTML = `
    <div class="card-title p-3 d-flex justify-content-between align-items-center">
        <h5 class="d-inline">${list.title}</h5>
        <div>
            <button class="btn btn-warning btn-edit-list" data-list="${
              list.title
            }">Edit</button>
            <button class="btn btn-danger btn-delete-list" data-list="${
              list.title
            }">Delete</button>
            </div>
          </div>
        <div class="card-task p-3">
          <div class="tasks-${list.title.replace(/\s/g, "-")}">
            <!-- Tasks will be dynamically added here -->
          </div>
        </div>
      `;
    tasksContainer.appendChild(listDiv);
    renderTasks(list.title);
  });

  // Add event listener for edit list button
  const editListButtons = document.querySelectorAll(".btn-edit-list");
  editListButtons.forEach((button) => {
    button.addEventListener("click", function () {
      const oldTitle = this.dataset.list;
      const newTitle = prompt("Enter new title for the list:");
      if (newTitle !== null) {
        editList(oldTitle, newTitle);
      }
    });
  });

  // Add event listener for delete list button
  const deleteListButtons = document.querySelectorAll(".btn-delete-list");
  deleteListButtons.forEach((button) => {
    button.addEventListener("click", function () {
      const listTitle = this.dataset.list;
      if (confirm("Are you sure you want to delete this list?")) {
        deleteList(listTitle);
      }
    });
  });
}

// Function to add a list
function addList(title) {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  lists.push({ title, tasks: [] });
  localStorage.setItem("lists", JSON.stringify(lists));
  renderListsAndSelectDropdown();
  renderLists();
}

// Function to add a task
function addTask(title, description, listTitle) {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const list = lists.find((list) => list.title === listTitle);
  if (list) {
    list.tasks.push({ title, description });
    localStorage.setItem("lists", JSON.stringify(lists));
    renderTasks(listTitle);
  }
}

// Function to render tasks for a specific list
function renderTasks(listTitle) {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const list = lists.find((list) => list.title === listTitle);
  if (list) {
    const tasksContainer = document.querySelector(
      `.tasks-${listTitle.replace(/\s/g, "-")}`
    );
    tasksContainer.innerHTML = "";
    list.tasks.forEach((task, index) => {
      const taskDiv = document.createElement("div");
      taskDiv.className = "mb-2";
      taskDiv.innerHTML = `
          <div class="card text-center mb-3" style="width: 18rem;">
          <div class="card-header">
            <h5>${task.title}</h5>
            </div>
            <div class="card-body">
            <p class="card-text">${task.description}</p>
            <button class="btn btn-warning btn-edit" data-list="${listTitle}" data-index="${index}">Edit</button>
              <button class="btn btn-danger btn-delete" data-list="${listTitle}" data-index="${index}">Delete</button>
            </div>
          </div>
        `;
      tasksContainer.appendChild(taskDiv);
    });
  }
}

// Function to delete a task
function deleteTask(listTitle, index) {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const listIndex = lists.findIndex((list) => list.title === listTitle);
  if (listIndex !== -1) {
    lists[listIndex].tasks.splice(index, 1);
    localStorage.setItem("lists", JSON.stringify(lists));
    renderTasks(listTitle);
  }
}

// Function to handle task edit
function editTask(listTitle, index) {
  const lists = JSON.parse(localStorage.getItem("lists")) || [];
  const listIndex = lists.findIndex((list) => list.title === listTitle);
  if (listIndex !== -1) {
    const newTitle = prompt("Enter new title:");
    const newDescription = prompt("Enter new description:");
    if (newTitle !== null && newDescription !== null) {
      lists[listIndex].tasks[index].title = newTitle;
      lists[listIndex].tasks[index].description = newDescription;
      localStorage.setItem("lists", JSON.stringify(lists));
      renderTasks(listTitle);
    }
  }
}

// Function to delete a list
function deleteList(listTitle) {
  let lists = JSON.parse(localStorage.getItem("lists")) || [];
  lists = lists.filter((list) => list.title !== listTitle);
  localStorage.setItem("lists", JSON.stringify(lists));
  renderListsAndSelectDropdown();
  renderLists();
}

// Function to edit a list
function editList(oldTitle, newTitle) {
  let lists = JSON.parse(localStorage.getItem("lists")) || [];
  const listIndex = lists.findIndex((list) => list.title === oldTitle);
  if (listIndex !== -1) {
    lists[listIndex].title = newTitle;
    localStorage.setItem("lists", JSON.stringify(lists));
    renderListsAndSelectDropdown();
    renderLists();
  }
}

// Event listener for list form submission
document
  .getElementById("listForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const listTitle = document.getElementById("listTitle").value;
    addList(listTitle);
    document.getElementById("listForm").reset();
  });

// Event listener for task form submission
document
  .getElementById("taskForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const title = document.getElementById("taskTitle").value;
    const description = document.getElementById("taskDescription").value;
    const listTitle = document.getElementById("taskList").value;
    addTask(title, description, listTitle);
    document.getElementById("taskForm").reset();
  });

// Event delegation for delete button
document
  .getElementById("tasksContainer")
  .addEventListener("click", function (event) {
    if (event.target.classList.contains("btn-delete")) {
      const listTitle = event.target.dataset.list;
      const index = parseInt(event.target.dataset.index);
      if (confirm("Are you sure you want to delete this task?")) {
        deleteTask(listTitle, index);
      }
    }
  });

// Event delegation for edit button
document
  .getElementById("tasksContainer")
  .addEventListener("click", function (event) {
    if (event.target.classList.contains("btn-edit")) {
      const listTitle = event.target.dataset.list;
      const index = parseInt(event.target.dataset.index);
      editTask(listTitle, index);
    }
  });

// Initial rendering
renderListsAndSelectDropdown();
renderLists();
